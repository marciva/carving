# carving

#### 介绍
2D雕刻机的控制移动APP源码 

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

####  **写给往后更新完善的人** 


这是一个毕业设计作品，运用到单片机，电子技术基础，android手机APP开发等技术
我没能将此项目完善到极致
所以我希望往后的小师弟或者小师妹如果有兴趣，又想拿一些项目练练手
这是个不错的项目，我会提供各类资料于你 

参考网址：
1.http://bbs.eeworld.com.cn/thread-645313-1-1.html (参考作品)
2.https://blog.csdn.net/weixin_40478003/article/details/83504867 (我的博客)   
3.链接：https://pan.baidu.com/s/1I88MUJd3QYdJGFW678ewcw 提取码：t9gg(毕业论文) 



#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)